# -*- coding: utf-8 -*-
from odoo import _, api, fields, models, fields


class Production(models.Model):  # TODO Check
    _inherit = 'mrp.production'

    sale_id = fields.Many2one(
        comodel_name='sale.order',
        compute='_get_sale_id',
        store=True,
    )
    varnish_front_id = fields.Many2one(
        related='product_id.varnish_front_id',
        readonly=True,
    )
    varnish_back_id = fields.Many2one(
        related='product_id.varnish_back_id',
        readonly=True,
    )
    hot_foil_id = fields.Many2one(
        related='product_id.hot_foil_id',
        readonly=True,
    )
    cold_foil_id = fields.Many2one(
        related='product_id.cold_foil_id',
        readonly=True,
    )
    emboss = fields.Boolean(
        related='product_id.emboss',
        readonly=True,
    )
    deboss = fields.Boolean(
        related='product_id.deboss',
        readonly=True,
    )

    @api.depends('origin')
    def _get_sale_id(self):
        for record in self:
            record.sale_id = self.env['sale.order'].search([('name', '=', record.origin)], limit=1)

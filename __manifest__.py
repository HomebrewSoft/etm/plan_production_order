{
    'name': 'plan_production_order',
    'version': '13.0.1.0.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'mrp',
        'sale',
        'product_prototype',
    ],
    "data": [
        # security
        # data
        # reports
        # views
        "views/mrp_production.xml",
    ],
}
